# Krisp
Krisp is a Lisp like language that is interpreted into [rust](https://www.rust-lang.org/). This is a placeholder readme. 
The below is a list of features I will include, and is not final. 
It will more than likely continue to grow.

## Features
- [x] Read a `.krisp` file.
  - [x] File passed via command line
  - [x] only `.krisp` files are allowed
- [x] Add support for printing a line to stdout
- [x] Add support for adding two integers
  - [x] Add support for the other math functions
- [ ] Add support for storing an integer into a variable
  - [ ] Previous abilities should support variables
- [ ] Add additional data types
  - [ ] `char`
  - [ ] `string`
  - [ ] `double`
  - [ ] `boolean`
- [ ] Add an `if` statement
## Possible Features
- [ ] Add support for functions
- [ ] Add support for recursion
- [ ] Better error messages
