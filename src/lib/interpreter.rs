use std::collections::VecDeque;
use std::convert::TryInto;
use std::fs::File;
use std::io::Read;
use std::path::Path;

use crate::lib::{
    ast::generate_ast,
    error::{Error, Result},
    tokens::Token
};

/// Takes in a string representation of a Krisp file and executes the code
pub fn run_file<P: AsRef<Path>>(file: P) -> Result<()> {
    let contents = get_file_contents(file)?;
    let tokens = tokenize(contents)?;
    let tree = generate_ast(tokens)?;
    tree.run()?;
    Ok(())
}

fn get_file_contents<P: AsRef<Path>>(path: P) -> Result<String> {
    match path.as_ref().extension().and_then(|ext| ext.to_str()) {
        Some("krisp") => {
            let mut file = File::open(&path).unwrap();
            let mut contents = String::new();
            file.read_to_string(&mut contents)?;
            Ok(contents)
        }
        _ => Err(Error::FileExtension(
            "File with `.krisp` extension expected".to_string(),
        )),
    }
}

fn tokenize(contents: String) -> Result<VecDeque<Token>> {
    if contents.is_empty() {
        return Err(Error::Parse("Cannot parse empty file".to_string()));
    }

    // Skip over all new line characters
    let contents = contents.trim().replace("\n", "").replace("\r", "");

    let mut chars = contents.chars();
    let mut string = String::new();
    let mut vec_deque = VecDeque::new();

    while let Some(inner) = chars.next() {
        if inner == '(' || inner == ')' {
            if !string.is_empty() {
                vec_deque.push_back(string.try_into()?);
                string = String::new()
            }
            vec_deque.push_back(inner.to_string().try_into()?)
        } else if inner == ' ' {
            if !string.is_empty() {
                vec_deque.push_back(string.try_into()?);
                string = String::new()
            }
        } else {
            string.push(inner);
        }
    }

    if !string.is_empty() {
        vec_deque.push_back(string.try_into()?);
    }

    Ok(vec_deque)
}

#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::Write;

    use tempfile::TempDir;

    use super::*;

    #[test]
    fn test_non_krisp_files_throw_error() {
        let dir = TempDir::new().unwrap();
        let path = dir.path().join("test.txt");
        File::create(&path).unwrap();
        let result = get_file_contents(path);
        assert!(result.is_err());
        assert_eq!(
            "File with `.krisp` extension expected",
            format!("{}", result.err().unwrap())
        );
    }

    #[test]
    fn test_krisp_file_is_read() {
        let dir = TempDir::new().unwrap();
        let path = dir.path().join("test.krisp");
        let file = File::create(&path)
            .unwrap()
            .write_all(b"Krisp file contents");
        drop(file);
        let result = get_file_contents(path);
        assert!(result.is_ok());
        assert_eq!("Krisp file contents".to_string(), result.ok().unwrap())
    }

    #[test]
    fn test_tokenize_empty_string() {
        let tokens = tokenize("".to_string());
        assert!(tokens.is_err());
        assert_eq!(
            "Cannot parse empty file".to_string(),
            format!("{}", tokens.err().unwrap())
        )
    }

    #[test]
    fn test_token_parses_simple_print() {
        let contents = "( print 5 )".to_string();
        let tokens = tokenize(contents);
        assert!(tokens.is_ok());
        assert_eq!(
            VecDeque::from(vec![
                Token::LeftParen,
                Token::Print,
                Token::Number(5.0),
                Token::RightParen
            ]),
            tokens.ok().unwrap()
        );
    }

    #[test]
    fn test_token_parses_simple_print_no_spaces() {
        let contents = "(print 5)".to_string();
        let tokens = tokenize(contents);
        assert!(tokens.is_ok());
        assert_eq!(
            VecDeque::from(vec![
                Token::LeftParen,
                Token::Print,
                Token::Number(5.0),
                Token::RightParen
            ]),
            tokens.ok().unwrap()
        );
    }
}
