use std::error::Error as ErrorTrait;
use std::fmt::Display;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    FileExtension(String),
    IO(std::io::Error),
    Parse(String),
    Regex(regex::Error),
    NumberParse(std::num::ParseFloatError, String),
    LogicError(String),
    SyntaxError(String),
}

impl From<std::io::Error> for Error {
    fn from(item: std::io::Error) -> Self {
        Error::IO(item)
    }
}

impl From<regex::Error> for Error {
    fn from(item: regex::Error) -> Self {
        Error::Regex(item)
    }
}

impl From<(std::num::ParseFloatError, String)> for Error {
    fn from((item, string): (std::num::ParseFloatError, String)) -> Self {
        Error::NumberParse(item, string)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use self::Error::*;

        match self {
            FileExtension(string) => write!(f, "{}", string),
            IO(io) => write!(f, "{}", io),
            Parse(parse) => write!(f, "{}", parse),
            Regex(regex) => write!(f, "{}", regex),
            NumberParse(float, string) => write!(f, "Could not parse: {}. {}", string, float),
            LogicError(error) => write!(f, "Logic error: {}", error),
            SyntaxError(error) => write!(f, "Syntax error: {}", error)
        }
    }
}

impl ErrorTrait for Error {
    fn source(&self) -> Option<&(dyn ErrorTrait + 'static)> {
        use self::Error::*;

        match self {
            FileExtension(_) => None,
            IO(io) => Some(io),
            Parse(_) => None,
            Regex(regex) => Some(regex),
            NumberParse(float, _) => Some(float),
            LogicError(_) => None,
            SyntaxError(_) => None
        }
    }
}
