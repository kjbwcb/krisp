use std::collections::VecDeque;
use std::io::{stdout, Write};

use crate::lib::error::{Error, Result};
use crate::lib::tokens::Token;

pub fn generate_ast(tokens: VecDeque<Token>) -> Result<AST> {
    let mut tokens = tokens;

    let mut statements = VecDeque::new();

    while !tokens.is_empty() {
        let (stmt, rest) = Statement::parse(tokens)?;
        if let Node::Statement(stmt) = stmt {
            statements.push_back(stmt);
        } else {
            return Err(Error::SyntaxError("Expected a Statement".to_string()));
        }
        tokens = rest;
    }

    Ok(AST { nodes: statements })
}

enum Node {
    Statement(Statement),
    Expression(Expression),
}

pub struct AST {
    nodes: VecDeque<Statement>,
}

impl AST {
    pub fn run(self) -> Result<()> {
        for node in self.nodes.into_iter() {
            node.run(&mut stdout())?
        }

        Ok(())
    }
}

enum Expression {
    Value(f64),
    Addition(Box<Expression>, Box<Expression>),
    Subtraction(Box<Expression>, Box<Expression>),
    Multiplication(Box<Expression>, Box<Expression>),
    Division(Box<Expression>, Box<Expression>),
    SquareRoot(Box<Expression>),
    Modular(Box<Expression>, Box<Expression>)
}

impl Expression {
    fn solve(self) -> Result<f64> {
        Ok(match self {
            Expression::Value(val) => val,
            Expression::Addition(left, right) => left.solve()? + right.solve()?,
            Expression::Subtraction(left, right) => left.solve()? - right.solve()?,
            Expression::Multiplication(left, right) => left.solve()? * right.solve()?,
            Expression::Division(left, right) => {
                let right = right.solve()?;
                if right == 0.0 {
                    return Err(Error::LogicError("Cannot divide by zero".to_string()));
                } else {
                    left.solve()? / right
                }
            },
            Expression::SquareRoot(expr) => {
                let expr = expr.solve()?;
                if expr >= 0.0 {
                    expr.sqrt()
                } else {
                    return Err(Error::LogicError("Cannot take square root of a negative number".to_string()))
                }
            },
            Expression::Modular(left, right) => left.solve()? % right.solve()?
        })
    }
}

enum Statement {
    Print(Expression),
}

impl Statement {
    pub fn run<T: Write>(self, write: &mut T) -> Result<()> {
        match self {
            Statement::Print(expr) => {
                writeln!(write, "{}", expr.solve()?);
                Ok(())
            }
        }
    }

    pub fn parse(mut tokens: VecDeque<Token>) -> Result<(Node, VecDeque<Token>)> {
        match tokens.pop_front() {
            Some(Token::LeftParen) => {
                if let Some(token) = tokens.pop_front() {
                    let (node, mut rest) = match token {
                        Token::Number(val) => {
                                (Node::Expression(Expression::Value(val)), tokens)
                        }
                        Token::Plus => {
                            let (left, rest) = Statement::parse(tokens)?;
                            let (right, mut rest) = Statement::parse(rest)?;
                            if let (Node::Expression(left), Node::Expression(right)) = (left, right) {
                                    (Node::Expression(Expression::Addition(Box::new(left), Box::new(right))), rest)
                            } else {
                                return Err(Error::SyntaxError("Addition is in invalid format".to_string()));
                            }
                        },
                        Token::Minus => {
                            let (left, rest) = Statement::parse(tokens)?;
                            let (right, mut rest) = Statement::parse(rest)?;
                            if let (Node::Expression(left), Node::Expression(right)) = (left, right) {
                                (Node::Expression(Expression::Subtraction(Box::new(left), Box::new(right))), rest)
                            } else {
                                return Err(Error::SyntaxError("Subtraction is in invalid format".to_string()));
                            }
                        },
                        Token::Multiplication => {
                            let (left, rest) = Statement::parse(tokens)?;
                            let (right, mut rest) = Statement::parse(rest)?;
                            if let (Node::Expression(left), Node::Expression(right)) = (left, right) {
                                (Node::Expression(Expression::Multiplication(Box::new(left), Box::new(right))), rest)
                            } else {
                                return Err(Error::SyntaxError("Multiplication is in invalid format".to_string()));
                            }
                        },
                        Token::Division => {
                            let (left, rest) = Statement::parse(tokens)?;
                            let (right, mut rest) = Statement::parse(rest)?;
                            if let (Node::Expression(left), Node::Expression(right)) = (left, right) {
                                (Node::Expression(Expression::Division(Box::new(left), Box::new(right))), rest)
                            } else {
                                return Err(Error::SyntaxError("Division is in invalid format".to_string()));
                            }
                        },
                        Token::SquareRoot => {
                            let (stmt, mut rest) = Statement::parse(tokens)?;
                            if let Node::Expression(expr) = stmt {
                                (Node::Expression(Expression::SquareRoot(Box::new(expr))), rest)
                            } else {
                                return Err(Error::SyntaxError("Square root is in invalid format".to_string()));
                            }
                        },
                        Token::Modular => {
                            let (left, rest) = Statement::parse(tokens)?;
                            let (right, mut rest) = Statement::parse(rest)?;
                            if let (Node::Expression(left), Node::Expression(right)) = (left, right) {
                                (Node::Expression(Expression::Modular(Box::new(left), Box::new(right))), rest)
                            } else {
                                return Err(Error::SyntaxError("Modular division is in invalid format".to_string()));
                            }
                        },
                        Token::Print => {
                            let (stmt, mut rest) = Statement::parse(tokens)?;
                            if let Node::Expression(expr) = stmt {
                                    (Node::Statement(Statement::Print(expr)), rest)
                            } else {
                                return Err(Error::SyntaxError("Cannot print this".to_string()))
                            }
                        }
                        _ => return Err(Error::SyntaxError("Invalid symbol".to_string()))
                    };
                    if let Some(Token::RightParen) = rest.pop_front() {
                        Ok((node, rest))
                    } else {
                        Err(Error::SyntaxError("Missing closing parenthesis".to_string()))
                    }
                } else {
                    Err(Error::SyntaxError("Application ends before expected.".to_string()))
                }
            }
            Some(Token::Number(num)) => Ok((Node::Expression(Expression::Value(num)), tokens)),
            _ => Err(Error::SyntaxError("Statement or expression does not begin with a left parenthesis".to_string()))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_statement_parse() {
        let tokens = VecDeque::from(vec![Token::LeftParen, Token::Plus, Token::Number(3.0), Token::Number(2.0), Token::RightParen]);

        let result = Statement::parse(tokens);

        assert!(result.is_ok());

        let (node, rest) = result.unwrap();

        if let Node::Expression(Expression::Addition(left, right)) = node {
            assert_eq!(3.0, left.solve().unwrap());
            assert_eq!(2.0, right.solve().unwrap());
            assert!(rest.is_empty())
        } else {
            panic!()
        }
    }
}
