use std::convert::TryFrom;

use regex::Regex;

use super::error::Error;

#[derive(PartialEq, Debug)]
pub enum Token {
    LeftParen,
    Print,
    Number(f64),
    RightParen,
    Plus,
    Minus,
    Multiplication,
    Division,
    SquareRoot,
    Modular,
}

impl TryFrom<String> for Token {
    type Error = Error;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        use self::Token::*;
        let number_reg_exp = Regex::new(r"^[-]?([0-9]+\.[0-9]+|[0-9]+)$")?;

        Ok(match value.as_str() {
            "(" => LeftParen,
            ")" => RightParen,
            string if number_reg_exp.is_match(string) => {
                Number(string.parse().map_err(|err| (err, string.to_string()))?)
            }
            "print" => Print,
            "+" => Plus,
            "-" => Minus,
            "*" => Multiplication,
            "/" => Division,
            "sqrt" => SquareRoot,
            "mod" => Modular,
            n => Err(Error::Parse(format!(
                "Could not parse as it is not a valid token: `{}`",
                n
            )))?,
        })
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryInto;

    use crate::lib::error::Result;

    use super::*;
    use super::Token::*;

    #[test]
    fn test_try_from_for_left_paren() {
        let token = "(".to_string().try_into().unwrap();
        assert_eq!(LeftParen, token);
    }

    #[test]
    fn test_try_from_for_right_paren() {
        let token = ")".to_string().try_into().unwrap();
        assert_eq!(RightParen, token);
    }

    #[test]
    fn test_try_from_for_print() {
        let token = "print".to_string().try_into().unwrap();
        assert_eq!(Print, token);
    }

    #[test]
    fn test_try_from_for_number() {
        let token = "5.3".to_string().try_into().unwrap();
        assert_eq!(Number(5.3f64), token);
    }

    #[test]
    fn test_handle_number_parse_error() {
        let token: Result<Token> = "5.".to_string().try_into();
        assert!(token.is_err());
        assert_eq!(
            "Could not parse as it is not a valid token: `5.`".to_string(),
            format!("{}", token.err().unwrap())
        );
    }

    #[test]
    fn test_try_from_returns_err() {
        let token: Result<Token> = "f".to_string().try_into();
        assert!(token.is_err());
        assert_eq!(
            "Could not parse as it is not a valid token: `f`".to_string(),
            format!("{}", token.err().unwrap())
        );
    }
}
