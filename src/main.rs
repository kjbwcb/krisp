#![allow(unused)]

use std::error::Error;

use clap::{App, Arg};

use lib::{error::Result, interpreter::run_file};

mod lib;

fn main() -> Result<()> {
    let matches = App::new("Krisp")
        .about("Runs `.krisp` files. Krisp is a Lisp like language that is interpreted into rust.")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Kurt Braun <kbraun9118@gmail.com>")
        .arg(
            Arg::with_name("INPUT")
                .help("Input file to run")
                .required(true)
                .index(1),
        )
        .get_matches();

    match matches.values_of("INPUT") {
        Some(file) => {
            if let Err(err) = run_file(file.collect::<String>()) {
                eprintln!("{}", err)
            }
        }
        None => eprintln!("No file passed to command"),
    }

    Ok(())
}
